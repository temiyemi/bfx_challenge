## Running 

See below for setting up Grenache and running grape servers.

With a running grape server, start 

```
node server.js
```

I implemented a single client file, however, for testing purposes, there's 3 client files with seeded order requests. Each can be started as follows:

```
node clients/client_1.js
```

```
node clients/client_2.js
```

```
node clients/client_3.js
```

## Missing

 - The clients should connect with the grape network via websocket
 - Each new order should be broadcasted to other clients via websocket
 - Each instance of the orderbook should listen to new order requests from other clients on the exchange network
 - For every new order request, 

### Setting up the DHT

```
npm i -g grenache-grape
```

```
# boot two grape servers

grape --dp 20001 --aph 30001 --bn '127.0.0.1:20002'
grape --dp 20002 --aph 40001 --bn '127.0.0.1:20001'
```

### Setting up Grenache in your project

```
npm install --save grenache-nodejs-http
npm install --save grenache-nodejs-link
```
