// This RPC server will announce itself as `exchange_server`
// in our Grape Bittorrent network
// When it receives requests, it will answer with 'world'
// an instance of the orderbook
'use strict'

const { PeerRPCServer }  = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')

const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCServer(link, {
  timeout: 300000
})
peer.init()

const port = 1024 + Math.floor(Math.random() * 1000)
const service = peer.transport('server')
service.listen(port)

setInterval(function () {
  link.announce('exchange_server', service.port, {})
}, 1000)

service.on('request', (rid, key, payload, handler) => {
  console.log('client.id', payload.client_id)
  // broadcast the new order request to connected peers
  const { order } = payload;
  // console.log(order)  
  // return
  handler.reply(null, payload)
})
