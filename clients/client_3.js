const orderbook = require('../client');

orderbook.sell({ currency: 'BTC/USD', volume: 60, rate: 12400, spread: 60 });

orderbook.buy({ currency: 'ETH/USD', volume: 200, rate: 630, spread: 35 });

orderbook.sell({ currency: 'ETH/USD', volume: 50, rate: 320, spread: 35 });

orderbook.snapshot()
