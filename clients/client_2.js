const orderbook = require('../client');

orderbook.buy({ currency: 'BTC/USD', volume: 80, rate: 12400, spread: 30 });

orderbook.buy({ currency: 'BTC/USD', volume: 120, rate: 12420, spread: 35 });

orderbook.sell({ currency: 'ETH/USD', volume: 500, rate: 655, spread: 20 });

orderbook.snapshot()