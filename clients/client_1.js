const orderbook = require('../client');

orderbook.sell({ currency: 'BTC/USD', volume: 100, rate: 12500, spread: 50 });

orderbook.buy({ currency: 'BTC/USD', volume: 100, rate: 12450, spread: 40 });

orderbook.buy({ currency: 'ETH/USD', volume: 350, rate: 658.5, spread: 26 });

orderbook.buy({ currency: 'BTC/USD', volume: 23, rate: 12480, spread: 60 });

orderbook.sell({ currency: 'ETH/USD', volume: 100, rate: 635, spread: 15 });

orderbook.buy({ currency: 'ETH/USD', volume: 150, rate: 300, spread: 50 });

orderbook.snapshot()