// This client will as the DHT for a service called `exchange_server`
// and then establishes a P2P connection it.
// It will then send order request to the RPC server, from within its orderbook instance

'use strict'

const { PeerRPCClient }  = require('grenache-nodejs-http')
const Link = require('grenache-nodejs-link')
const OrderBook = require('./exchange/orderbook')

const link = new Link({
  grape: 'http://127.0.0.1:30001'
})
link.start()

const peer = new PeerRPCClient(link, {})
peer.init()

const orderbook = new OrderBook(peer);

module.exports = orderbook;
