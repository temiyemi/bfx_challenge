class OrderMatcher {
  constructor (order, orders) {
    this.order = order;
    this.orders = orders;
  }

  match() {
    const { type, volume, lowest_rate, highest_rate, rate } = this.order;
    this.orders.forEach(order => {
      // find a match on exchange rate first
      if (order.lowest_rate >= order.lowest_rate || order.rate <= rate || order.highest_rate <= highest_rate) {
        // continue;
      }
      if (order.volume >= volume) {

      }
      // check for an exchange rate and spread for the order
      // if it's a sell order -> we cannot sell more than the volume we have
      // if it's a buy order -> we cannot buy more than the volume we have       
    })
  }
}

module.exports = OrderMatcher;