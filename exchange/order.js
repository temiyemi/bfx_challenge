class Order {
  /** 
   * Order instance constructor
   * @param volume {number} the total volume of currency to exchange
   * @param rate {number} the preferred exchange rate
   * @param type {string} the type of exchange (buy|sell)
   * @param currency {string} the currency to exchange
   * @param spread {number} the spread +/- from the exchange rate
   * @param status {string} the status of the transaction (pending|processing|processed|cancelled)
  */
  constructor(id, volume, rate, type, currency, spread, status) {
    this.id = id;
    this.volume = volume;
    this.currency = currency;
    this.type = type;
    this.rate = rate;
    if (spread) {
      this.lowest_rate = rate - spread;
      this.highest_rate = rate + spread;
    }
    if (type === 'buy') {
      this.bid = rate;
    } else if (type === 'sell') {
      this.ask = rate;
    }
    this.spread = spread;
    this.status = status;
    this.time = new Date();
  }

}

module.exports = Order;