const Order = require('./order');
const OrderMatcher = require('./order-matcher');

class OrderBook {
  
  orders = []
  
  /** 
   * Instantiate a new OrderBook instance on the client
   * @param peer instance of the client's PeerRPCClient
  */
  
  constructor(peer) {
    this.peer = peer;
    this.client_id = Math.floor(Math.random() * 1000);
  }

  /** 
   * Submits an order to buy currency pair
   * @param rate {number} the (preferred) exchange rate at which to buy
   * @param volume {number} the total volume of currency to buy
   * @param currency {string} the currency to buy
   * @param spread {number} (optional) +/- offset from the exchange rate at which to buy  
   * @returns Promise<data|error>
  */
  buy ({ rate, volume, currency, spread }) {
    const id = this.orders.length; // + 1;
    const order = new Order(id, volume, rate, 'buy', currency, spread, 'pending')
    this.addNewOrder(order)
  }

  /** 
   * Submits an order to sell currency pair
   * @param rate {number} the (preferred) exchange rate at which to sell
   * @param volume {number} the total volume of currency available to sell
   * @param currency {string} the currency to sell
   * @param spread {number} (optional) +/- offset from the exchange rate at which to sell
   * @returns Promise<data|error>
  */
  sell ({ rate, volume, currency, spread }) {
    const id = this.orders.length; // + 1;
    const order = new Order(id, volume, rate, 'sell', currency, spread, 'pending')
    this.addNewOrder(order)
  }

  /** 
   * addNewOrder
   * @description Adds the new order to the lists of orders on the local exchange orderbook
   * then sends it to the server for broadcast to other peers
   * @param order {order} the (preferred) exchange rate at which to sell
   * @param volume {number} the total volume of currency available to sell
   * @param currency {string} the currency to sell
   * @param spread {number} (optional) +/- offset from the exchange rate at which to sell
   * @returns Promise<data|error>
  */
  addNewOrder(order) {
    // if there's an existing matching order in this orderbook
    // match and resolve the order
    // console.log(this.orders.length);
    const pending_orders = this.orders.slice(0).filter(({ currency, type, status }) => {
      return currency === order.currency && type !== order.type && status === 'pending'
    })
    if (pending_orders.length > 0) {
      // console.log('found possible matching orders', pending_orders.length)
      const matcher = new OrderMatcher(order, pending_orders);
      // const orders = matcher.match();
      
    }
    else {
      this.orders.push(order);
      
    }
    const client_id = this.client_id;
    this.peer.map('exchange_server', { order, client_id }, { timeout: 10000 }, (err, data) => {
      if (err) {
        console.error(err)
        // process.exit(-1)
      }
      // console.log(data) // instance of the orderbook
    })
    // notify the network by broadcasting the order for matching by other peers
    // otherwise broadcast the order
  }

  /** 
   * Console.log the current orders in the order book 
  */
  snapshot() {
    setInterval(() => {
      console.log(this.orders)
    }, 1000)
  }

}

module.exports = OrderBook;
